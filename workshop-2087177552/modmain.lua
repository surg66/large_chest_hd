local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS
local RECIPETABS = GLOBAL.RECIPETABS
local Ingredient = GLOBAL.Ingredient
local TECH = GLOBAL.TECH
local TUNING = GLOBAL.TUNING

PrefabFiles =
{
    "largechest",
    "largeicebox",
    "largesaltbox",
}

Assets =
{
    Asset("ATLAS", "images/inventoryimages/largechest.xml"),
    Asset("IMAGE", "images/inventoryimages/largechest.tex"),
    Asset("ANIM", "anim/ui_largechest_5x5.zip"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_icebox_coffin.zip"),
    Asset("PKGREF", "anim/dynamic/large_icebox_coffin.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_icebox_crystal.zip"),
    Asset("PKGREF", "anim/dynamic/large_icebox_crystal.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_icebox_porcelain.zip"),
    Asset("PKGREF", "anim/dynamic/large_icebox_porcelain.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_icebox_victorian.zip"),
    Asset("PKGREF", "anim/dynamic/large_icebox_victorian.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_icebox_kitchen.zip"),
    Asset("PKGREF", "anim/dynamic/large_icebox_kitchen.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_saltbox_nautical.zip"),
    Asset("PKGREF", "anim/dynamic/large_saltbox_nautical.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_saltbox_shaker.zip"),
    Asset("PKGREF", "anim/dynamic/large_saltbox_shaker.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_saltbox_victorian.zip"),
    Asset("PKGREF", "anim/dynamic/large_saltbox_victorian.dyn"),
    Asset("DYNAMIC_ANIM", "anim/dynamic/large_saltbox_kitchen.zip"),
    Asset("PKGREF", "anim/dynamic/large_saltbox_kitchen.dyn"),
}

local recipe_difficulty = GetModConfigData("OPT_DIFFICULTY")
local containers = require "containers"
local containers_widgetsetup_base = containers.widgetsetup
local params = {}

local function MakeContainer()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_largechest_5x5",
            animbuild = "ui_largechest_5x5",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }

    for y = 3, -1, -1 do
        for x = -1, 3 do
            table.insert(container.widget.slotpos, GLOBAL.Vector3(80 * x - 80 * 2 + 80, 80 * y - 80 * 2 + 80, 0))
        end
    end

    return container
end

function containers.widgetsetup(container, prefab, data, ...)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end

        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
    else
        containers_widgetsetup_base(container, prefab, data, ...)
    end
end

--LARGE CHEST
if GetModConfigData("CHEST_ENABLE") then
    params.largechest = MakeContainer()

    local ingredients = (recipe_difficulty == 2) and { Ingredient("boards", 8), Ingredient("goldnugget", 4) } or
                        (recipe_difficulty == 1) and { Ingredient("boards", 8), Ingredient("goldnugget", 2) } or
                        (recipe_difficulty == 0) and { Ingredient("boards", 6) } or
                        print "ERROR: unsupported recipe difficulty" and nil

    local recipe = AddRecipe2("largechest", ingredients, TECH.SCIENCE_TWO,
                              {
                                   placer="largechest_placer",
                                   min_spacing = 2.8,
                                   atlas = "images/inventoryimages/largechest.xml",
                                   image = "large_chest.tex",
                              },
                              {"STRUCTURES", "CONTAINERS"})

    if GLOBAL.AllRecipes.treasurechest ~= nil then
        recipe.sortkey = GLOBAL.AllRecipes.treasurechest.sortkey + 0.1
    end

    STRINGS.NAMES.LARGECHEST = "Large Chest"
    STRINGS.RECIPE_DESC.LARGECHEST = "A large chest to put more stuff in"
    STRINGS.CHARACTERS.GENERIC.DESCRIBE.LARGECHEST = "Looks so fancy!"
end

--LARGE ICEBOX
if GetModConfigData("ICEBOX_ENABLE") then
    params.largeicebox = MakeContainer()

    function params.largeicebox.itemtestfn(container, item, slot)
        if item:HasTag("icebox_valid") then
            return true
        end

        --perishable
        if not (item:HasTag("fresh") or item:HasTag("stale") or item:HasTag("spoiled")) then
            return false
        end

        if item:HasTag("smallcreature") then
            return false
        end

        --edible
        for k, v in pairs(GLOBAL.FOODTYPE) do
            if item:HasTag("edible_"..v) then
                return true
            end
        end

        return false
    end

    local ingredients = (recipe_difficulty == 2) and { Ingredient("goldnugget", 8), Ingredient("gears", 3), Ingredient("boards", 4) } or
                        (recipe_difficulty == 1) and { Ingredient("goldnugget", 6), Ingredient("gears", 3), Ingredient("boards", 3) } or
                        (recipe_difficulty == 0) and { Ingredient("goldnugget", 4), Ingredient("gears", 2), Ingredient("boards", 2) } or
                        print "ERROR: unsupported recipe difficulty" and nil

    local recipe = AddRecipe2("largeicebox", ingredients, TECH.SCIENCE_TWO,
                              {
                                   placer="largeicebox_placer",
                                   min_spacing = 2.5,
                                   atlas = "images/inventoryimages/largechest.xml",
                                   image = "large_icebox.tex",
                              },
                              {"STRUCTURES", "CONTAINERS", "COOKING"})

    if GLOBAL.AllRecipes.icebox ~= nil then
        recipe.sortkey = GLOBAL.AllRecipes.icebox.sortkey + 0.1
    end

    STRINGS.NAMES.LARGEICEBOX = "Large Icebox"
    STRINGS.RECIPE_DESC.LARGEICEBOX = "A gigantic icebox."
    STRINGS.CHARACTERS.GENERIC.DESCRIBE.LARGEICEBOX = "I have harnessed the power of cold!"
end

--LARGE SALTBOX
if GetModConfigData("SALTBOX_ENABLE") then
    params.largesaltbox = MakeContainer()

    function params.largesaltbox.itemtestfn(container, item, slot)
        return ((item:HasTag("fresh") or item:HasTag("stale") or item:HasTag("spoiled"))
                 and item:HasTag("cookable")
                 and not item:HasTag("deployable")
                 and not item:HasTag("smallcreature")
                 and item.replica.health == nil
               ) or item:HasTag("saltbox_valid")
    end

    local ingredients = (recipe_difficulty == 2) and { Ingredient("saltrock", 28), Ingredient("bluegem", 4), Ingredient("cutstone", 4) } or
                        (recipe_difficulty == 1) and { Ingredient("saltrock", 24), Ingredient("bluegem", 3), Ingredient("cutstone", 3) } or
                        (recipe_difficulty == 0) and { Ingredient("saltrock", 16), Ingredient("bluegem", 2), Ingredient("cutstone", 2) } or
                        print "ERROR: unsupported recipe difficulty" and nil

    local recipe = AddRecipe2("largesaltbox", ingredients, TECH.SCIENCE_TWO,
                              {
                                   placer="largesaltbox_placer",
                                   min_spacing = 2.8,
                                   atlas = "images/inventoryimages/largechest.xml",
                                   image = "large_saltbox.tex",
                              },
                              {"STRUCTURES", "CONTAINERS", "COOKING"})

    if GLOBAL.AllRecipes.saltbox ~= nil then
        recipe.sortkey = GLOBAL.AllRecipes.saltbox.sortkey + 0.1
    end

    STRINGS.NAMES.LARGESALTBOX = "Large Saltbox"
    STRINGS.RECIPE_DESC.LARGESALTBOX = "Even saltier than the previous one"
    STRINGS.CHARACTERS.GENERIC.DESCRIBE.LARGESALTBOX = "It's worth more than its weight in gold!"
end

for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end

local ICEBOX_SKINS =
{
    "icebox_coffin",
    "icebox_crystal",
    "icebox_kitchen",
    "icebox_porcelain",
    "icebox_victorian",
}

local SALTBOX_SKINS =
{
    "saltbox_kitchen",
    "saltbox_nautical",
    "saltbox_shaker",
    "saltbox_victorian",
}

AddPrefabPostInit("reskin_tool", function(inst)
    if inst.components ~= nil and inst.components.spellcaster ~= nil then
        local original_CanCast = inst.components.spellcaster.can_cast_fn
        local original_Spell = inst.components.spellcaster.spell

        local function PuffFX(tool, target, scale, offset)
            local fx_pos_x, fx_pos_y, fx_pos_z = target.Transform:GetWorldPosition()
            local fx = GLOBAL.SpawnPrefab("explode_reskin")

            fx.Transform:SetScale(scale, scale, scale)
            fx.Transform:SetPosition(fx_pos_x, fx_pos_y + (offset or 0), fx_pos_z)
        end

        local function CanCast(doer, target, pos)
            if target == nil then
                return original_CanCast(doer, target, pos)
            end

            if target.prefab == "largeicebox" then
                for _, name in ipairs(ICEBOX_SKINS) do
                    if GLOBAL.TheInventory:CheckClientOwnership(doer.userid, name) then
                        return true
                    end
                end
            end

            if target.prefab == "largesaltbox" then
                for _, name in ipairs(SALTBOX_SKINS) do
                    if GLOBAL.TheInventory:CheckClientOwnership(doer.userid, name) then
                        return true
                    end
                end
            end

            return original_CanCast(doer, target, pos)
        end

        local function Spell(tool, target, pos)
            if target == nil then
                return original_Spell(tool, target, pos)
            end

            if target.prefab == "largeicebox" then
                local available_skins = {"icebox"}

                if tool._cached_reskinname["largeicebox"] == nil then
                    tool._cached_reskinname["largeicebox"] = available_skins[1]
                end

                local skin = tool._cached_reskinname["largeicebox"]

                for _, name in ipairs(ICEBOX_SKINS) do
                    if GLOBAL.TheInventory:CheckClientOwnership(tool.parent.userid, name) then
                        table.insert(available_skins, name)
                    end
                end

                local index = 1
                for i, name in ipairs(available_skins) do
                    if name == target._skin then
                        index = i
                        break
                    end
                end

                skin = available_skins[index + 1]
                if skin == nil then
                    skin = available_skins[1]
                end

                tool._cached_reskinname["largeicebox"] = skin

                target:SetSkin(skin)
                PuffFX(tool, target, 1.5, 1)

            elseif target.prefab == "largesaltbox" then
                local available_skins = {"saltbox"}

                if tool._cached_reskinname["largesaltbox"] == nil then
                    tool._cached_reskinname["largesaltbox"] = available_skins[1]
                end

                local skin = tool._cached_reskinname["largesaltbox"]

                for _, name in ipairs(SALTBOX_SKINS) do
                    if GLOBAL.TheInventory:CheckClientOwnership(tool.parent.userid, name) then
                        table.insert(available_skins, name)
                    end
                end

                local index = 1
                for i, name in ipairs(available_skins) do
                    if name == target._skin then
                        index = i
                        break
                    end
                end

                skin = available_skins[index + 1]
                if skin == nil then
                    skin = available_skins[1]
                end

                tool._cached_reskinname["largesaltbox"] = skin

                target:SetSkin(skin)
                PuffFX(tool, target, 1.5, 1)
            else
                original_Spell(tool, target, pos)
            end
        end

        inst.components.spellcaster:SetSpellFn(Spell)
        inst.components.spellcaster:SetCanCastFn(CanCast)
    end
end)
