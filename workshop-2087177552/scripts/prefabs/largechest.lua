local LARGE_CHEST_SCALE = 2
local original_onburnt

local function onburnt(inst)
    inst.AnimState:SetBank("chest")
    inst.AnimState:SetBuild("treasure_chest")
    inst.AnimState:SetScale(LARGE_CHEST_SCALE, LARGE_CHEST_SCALE)
    original_onburnt(inst)
end

local function fn()
    local inst = Prefabs.treasurechest.fn()

    inst.AnimState:SetBank("pandoras_chest_large")
    inst.AnimState:SetBuild("pandoras_chest_large")
    inst.AnimState:HideSymbol("sparkle")

    inst.MiniMapEntity:SetIcon("minotaurchest.png")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.container:WidgetSetup("largechest")

    original_onburnt = inst.components.burnable.onburnt
    inst.components.burnable:SetOnBurntFn(onburnt)

    return inst
end

return Prefab("largechest", fn),
       MakePlacer("largechest_placer", "pandoras_chest_large", "pandoras_chest_large", "closed", nil, nil, nil)
