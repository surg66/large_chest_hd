local LARGE_ICEBOX_SCALE = 1.5

local function OnSave(inst, data)
    data.skin = inst._skin or "icebox"
end

local function OnLoad(inst, data)
    if data ~= nil then
        inst._skin = data.skin or "icebox"
    end

    if inst._skin == nil then
        inst._skin = "icebox"
    end

    inst:SetSkin(inst._skin)
end

local function OnOpen(inst)
    local x, y, z = inst.Transform:GetWorldPosition()

    if inst._skin == "icebox_coffin" then
        local t = GetTime()
        if t >= (inst._open_fx_time or 0) then
            inst._open_fx_time = t + 1.3

            local fx = SpawnPrefab("icebox_coffin_bat_fx")

            fx.Transform:SetPosition(x, y, z)
            fx.Transform:SetScale(LARGE_ICEBOX_SCALE, LARGE_ICEBOX_SCALE, LARGE_ICEBOX_SCALE)
        end
    elseif inst._lc_fx == nil then
        if inst._skin == "icebox_crystal" then
            inst._lc_fx = SpawnPrefab("icebox_crystal_fx")
        elseif inst._skin == "icebox_porcelain" then
            inst._lc_fx = SpawnPrefab("icebox_porcelain_fx")
        elseif inst._skin == "icebox_victorian" then
            inst._lc_fx = SpawnPrefab("icebox_victorian_frost_fx")
        end

        if inst._lc_fx ~= nil then
            inst._lc_fx.Transform:SetPosition(x, y, z) 
            inst._lc_fx.AnimState:OverrideSymbol("cold_air", "large_"..inst._skin, "cold_air")
            inst._lc_fx.AnimState:OverrideSymbol("blink_dot", "large_"..inst._skin, "blink_dot")
            inst._lc_fx.AnimState:SetScale(LARGE_ICEBOX_SCALE, LARGE_ICEBOX_SCALE)
        end
    end
end

local function OnClosed(inst)
    if inst._lc_fx ~= nil then
        inst._lc_fx:Kill()
        inst._lc_fx = nil
    end
end

local function SetSkin(inst, name)
    inst._skin = name

    if inst._skin == "icebox" then
        inst.AnimState:SetBuild("ice_box")
    else
        inst.AnimState:SetBuild("large_"..inst._skin)
    end

    if inst.AnimState:IsCurrentAnimation("open") then
        OnClosed(inst)
        OnOpen(inst)
    end
end

local function fn()
    local inst = Prefabs.icebox.fn()

    inst.AnimState:SetScale(LARGE_ICEBOX_SCALE, LARGE_ICEBOX_SCALE)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.container:WidgetSetup("largeicebox")
    inst._skin = "icebox"

    inst:ListenForEvent("onopen", OnOpen)
    inst:ListenForEvent("onclose", OnClosed)
    inst:ListenForEvent("onremove", OnClosed)

    inst.SetSkin = SetSkin
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("largeicebox", fn),
       MakePlacer("largeicebox_placer", "icebox", "ice_box", "closed", nil, nil, nil, LARGE_ICEBOX_SCALE)
