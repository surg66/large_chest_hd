local LARGE_SALTBOX_SCALE = 1.5

local function OnSave(inst, data)
    data.skin = inst._skin or "saltbox"
end

local function OnLoad(inst, data)
    if data ~= nil then
        inst._skin = data.skin or "saltbox"
    end

    if inst._skin == nil then
        inst._skin = "saltbox"
    end

    inst:SetSkin(inst._skin)
end

local function SetSkin(inst, name)
    inst._skin = name
    if inst._skin == "saltbox" then
        inst.AnimState:SetBuild("saltbox")
    else
        inst.AnimState:SetBuild("large_"..inst._skin)
    end
end

local function fn()
    local inst = Prefabs.saltbox.fn()

    inst.AnimState:SetScale(LARGE_SALTBOX_SCALE, LARGE_SALTBOX_SCALE)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.container:WidgetSetup("largesaltbox")
    inst._skin = "saltbox"

    inst.SetSkin = SetSkin
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("largesaltbox", fn),
       MakePlacer("largesaltbox_placer", "saltbox", "saltbox", "closed", nil, nil, nil, LARGE_SALTBOX_SCALE)
